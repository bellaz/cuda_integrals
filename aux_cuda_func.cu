/*
*    aux_cuda_func.cu
*    Copyright (C) 2014  Bellandi Andrea
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "integral_interchange.h"

#define MAX(x,y) (x > y) ? x : y;
#define MIN(x,y) (x < y) ? x : y;

void test_cuda_error(struct integral_return *ir, int line, char * file){

  cudaError_t error_c  = cudaGetLastError();

  if(error_c != cudaSuccess)
    {
      ir->error = STATUS_ERROR | STATUS_CUDA_ERROR;
      // print the CUDA error message and exit
      printf("CUDA error called at %d line of %s: %s", errno, line, cudaGetErrorString(error_c));
    }
}

__device__ struct ndim_point eval_bin1D(long long int val){

  struct ndim_point result;
  double tmp;
  int i;

  const double integral_intervals[INT_DIMENSIONS * 2]  = INT_INTERVALS;
  const double bin_dims[INT_DIMENSIONS] = INT_BIN_DIMS;
  const long long int    div_numbers[INT_DIMENSIONS] = INT_DIV_NUMBERS;

  for(i = 0; i < INT_DIMENSIONS; i++)
    { 
      tmp = bin_dims[i] * ((val / div_numbers[i]) % INT_BINS);
      result.point[i] = integral_intervals[2 * i] + tmp;
    }

  return result;

}


__device__ double eval_func(struct ndim_point val){

  int i;
  double arg_fun[INT_DIMENSIONS];
  for(i = 0; i < INT_DIMENSIONS; i++) arg_fun[i] = val.point[i];

  return INT_FORMULA;
}

__device__ double atomicAdd(double* address, double val){

unsigned long long int* address_as_ull = (unsigned long long int*)address;
unsigned long long int old = *address_as_ull, assumed;

    do {
    	assumed = old;
    	old = atomicCAS(address_as_ull, assumed,__double_as_longlong(val + __longlong_as_double(assumed)));
    } while (assumed != old);
    
return __longlong_as_double(old);

}

__device__ double atomicMin(double* address, double val){

unsigned long long int* address_as_ull = (unsigned long long int*)address;
unsigned long long int old = *address_as_ull, assumed;

    do {
    	assumed = old;
    	old = atomicCAS(address_as_ull, assumed, __double_as_longlong(fmin(val,__longlong_as_double(assumed))));
    } while (assumed != old);
    
return __longlong_as_double(old);

}

__device__ double atomicMax(double* address, double val){

unsigned long long int* address_as_ull = (unsigned long long int*)address;
unsigned long long int old = *address_as_ull, assumed;

    do {
    	assumed = old;
    	old = atomicCAS(address_as_ull, assumed,__double_as_longlong(fmax(val,  __longlong_as_double(assumed))));
    } while (assumed != old);
    
return __longlong_as_double(old);

}

__device__ double theta(double x){

  return ((x > 0.0) ? 1.0 : 0.0);
}

__device__ double roundzero(double x){

  return ((x > 0.0) ? x : 0.0);
}


__device__ struct ndim_point_l get_coords(long long int idx){

  int i;
  const long long int div_numbers[INT_DIMENSIONS] = INT_DIV_NUMBERS;

  struct ndim_point_l result;
  for(i = 0; i < INT_DIMENSIONS; i++) result.point[i] = ((idx / div_numbers[i]) % INT_BINS);
  
  return result;
}
