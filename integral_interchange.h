/*
*    integral_interchange.h
*    Copyright (C) 2014  Bellandi Andrea
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INTEGRAL_INTERCHANGE
#define INTEGRAL_INTERCHANGE

#define ERROR_CHARACTERS 1024

#define INT_REAL_INTEGRATION
#ifdef INT_REAL_INTEGRATION
#include "int_params.h"
#else
#include "int_example_params.h"
#endif

#define EPS 10e-20 // this number can be changed. it is used as the minimum value of std

#ifdef INT_CUDA_SOURCE

#include <cuda_runtime.h>
#include <curand_kernel.h>

#endif

#ifdef INT_PTHREAD_SOURCE
#include <pthread.h>
#endif

#if (defined __cplusplus || defined INT_CUDA_SOURCE)
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <ctime>
#else 
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#endif

#include <sys/time.h>

#define STATUS_OK 0x00 // all ok
#define STATUS_ERROR 0x01 // generic error
#define STATUS_CUDA_ERROR 0x02 // error on cuda api
#define STATUS_MALLOC_ERROR 0x04 // error on memory allocation
#define STATUS_MATH_ERROR 0x08 // error on math operation
#define STATUS_TIME_ERROR 0x10 // timeout error


struct integral_return 
{
  double result; // result of the first integral pass
  double err;    // error of the first integral pass, calculated as the standard deviation
  long long int msec;      // time spent on the first pass
  long long int number_of_points;


  int number_of_passes; // number of passes
  
  unsigned char error; // error char, see below

};

struct ndim_point
{
  double point[INT_DIMENSIONS];
};


struct ndim_point_l
{
  long long int point[INT_DIMENSIONS];
};

#if (defined __cplusplus || defined INT_CUDA_SOURCE)
extern "C" {
#endif 
 
  struct integral_return integral_function(struct integral_return); // declaration of the integration function to
  void test_time_error(int error,struct integral_return *ir, int line, char * file);
  void test_malloc_error(void * p, struct integral_return *ir, int line, char * file);
  long timevaldiff(struct timeval *starttime, struct timeval *finishtime);// in a particular module  
  struct ndim_point_l get_coords_global(long long int idx);
  struct ndim_point eval_bin1D_global(long long int val);
  double eval_func_global(struct ndim_point val);
  double theta_global(double x);
  double roundzero_global(double x);

#if (defined __cplusplus || defined INT_CUDA_SOURCE)
}
#endif

#define CHECK_ERRORS(ir) if(ir.error & STATUS_ERROR) return ir;

#ifdef INT_CUDA_SOURCE


void test_cuda_error( struct integral_return *ir, int line, char * file); //test cuda errors

__device__ struct ndim_point eval_bin1D(long long int val);
__device__ double eval_func(struct ndim_point val);
__device__ double atomicAdd(double* address, double val);
__device__ double atomicMin(double* address, double val);
__device__ double atomicMax(double* address, double val);
__device__ double theta(double x);
__device__ double roundzero(double x);
__device__ struct ndim_point_l get_coords(long long int idx);

#else


#define get_coords(x) get_coords_global(x);
#define eval_bin1D(x) eval_bin1D_global(x);
#define eval_func(x)  eval_func_global(x); 
#define theta(x)      theta_global(x);
#define roundzero(x)  roundzero_global(x);

#endif

#endif
