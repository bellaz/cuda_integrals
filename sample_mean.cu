/*
*    Sample-Mean integration module
*    Copyright (C) 2014  Bellandi Andrea
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "integral_interchange.h"

#define MAX_BLOCKS 4096LL
#define BLOCK_SIZE 64
#define MIN(x,y) (x < y) ? x : y

//#define CUDA_RAND_TYPE curandStateMRG32k3a_t 
//#define CUDA_RAND_TYPE curandStatePhilox4_32_10_t  
#define CUDA_RAND_TYPE curandStateXORWOW_t 


CUDA_RAND_TYPE * dev_rand_states;


__global__ void initialize_random_gens(CUDA_RAND_TYPE * rand_states, long int seed){

  CUDA_RAND_TYPE state;
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  curand_init(seed, index, 0, &state);
  rand_states[index] = state; 

}


__global__ void integral_kernel(double * result, double * std, CUDA_RAND_TYPE * rand_states,long long int offset, long long int n){
  

  __shared__ double intermediate_result[BLOCK_SIZE];
  __shared__ double intermediate_std[BLOCK_SIZE];

  CUDA_RAND_TYPE state;

  int tid   = threadIdx.x;
  int j;
  long long int i;
  int aidx = blockIdx.x * blockDim.x + threadIdx.x;
  long long int idx = aidx;
  idx += offset;

  double tmp;
  const double intervals[INT_DIMENSIONS * 2] = INT_INTERVALS;
  
  intermediate_result[tid] = 0.0;
  intermediate_std[tid] = 0.0;

  struct ndim_point val_point;



  if(idx < n){

    state  = rand_states[aidx];
    for(i = 0; i < INT_ELMUL; i++){

      for(j = 0; j < INT_DIMENSIONS; j++){

	val_point.point[j] = curand_uniform_double(&state);
	val_point.point[j] *= (intervals[j * 2 + 1] - intervals[j * 2]);
	val_point.point[j] += intervals[j * 2];
      }

      tmp = eval_func(val_point);
      intermediate_result[tid] += tmp;
      intermediate_std[tid] += (tmp * tmp);
    }

    rand_states[aidx] = state;
  }
	

  __syncthreads();

  for(j = BLOCK_SIZE/2; j>0; j>>=1) {
      if(tid < j){
	  intermediate_result[tid] += intermediate_result[tid+j];
	  intermediate_std[tid] += intermediate_std[tid+j];
      }
      __syncthreads();
  }
  
  if(tid == 0){
    atomicAdd(result, intermediate_result[0]);
    atomicAdd(std, intermediate_std[0]);
  }
}


struct integral_return integral_function(struct integral_return integral_result)
{
  int error;
  long long int nblocks, offset;

  double result = 0.0, * devresult;
  double std = 0.0, *devstd;

  struct timeval msec_init, msec_end;
  
  integral_result.error = STATUS_OK;

  error = gettimeofday(&msec_init,NULL);
  test_time_error(error, &integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)	


  cudaMalloc((void **)&devresult, sizeof(double));
  cudaMalloc((void **)&devstd, sizeof(double));
  cudaMalloc(&dev_rand_states, sizeof(CUDA_RAND_TYPE) * BLOCK_SIZE * MAX_BLOCKS);
  
  initialize_random_gens<<< MAX_BLOCKS, BLOCK_SIZE >>>(dev_rand_states, time(NULL));

  test_cuda_error(&integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)
 
  cudaMemset((void*)devresult, 0x00, sizeof(double));  
  cudaMemset(devstd, 0x00, sizeof(double));

  test_cuda_error(&integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)

    
    for(offset = 0; offset < INT_TOTAL_BINS; offset += MAX_BLOCKS * BLOCK_SIZE){
  
      nblocks = MIN(MAX_BLOCKS, (INT_TOTAL_BINS - offset - 1)/BLOCK_SIZE + 1);

    
      integral_kernel<<< nblocks, BLOCK_SIZE >>> (devresult, devstd, dev_rand_states, offset,INT_TOTAL_BINS); 

      cudaDeviceSynchronize(); 
    }

  cudaMemcpy ((void*) &result,(void*) devresult, sizeof(double), cudaMemcpyDeviceToHost);
  cudaMemcpy ((void*) &std,(void*) devstd, sizeof(double), cudaMemcpyDeviceToHost);

  test_cuda_error(&integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)

  result *=  INT_BIN_IPERVOLUME;
  result /=  INT_ELMUL;
  std *=  INT_BIN_IPERVOLUME * INT_BIN_IPERVOLUME;
  std /=  INT_ELMUL;

  std *= INT_TOTAL_BINS;
  std = sqrt(fabs(std - (result * result))/((double)((INT_TOTAL_BINS * INT_ELMUL) - 1)));

  integral_result.result = result;
  integral_result.err = std;
  
  error = gettimeofday(&msec_end,NULL);
  test_time_error(error, &integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)

  
  integral_result.msec = timevaldiff(&msec_init, &msec_end);
  
  integral_result.number_of_passes = 1;
  
  integral_result.number_of_points = INT_TOTAL_BINS * INT_ELMUL;

  cudaFree(devresult);
  cudaFree(devstd);
  cudaFree(dev_rand_states);

  return integral_result;
}
