/*
*    int_params.h
*    Copyright (C) 2014  Bellandi Andrea
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// This module must be changed in order to change the integration
#ifndef INT_PARAMS
#define INT_PARAMS

// dimensions of the integral
#define INT_DIMENSIONS 2

// describe a 2D Gaussian
#define INT_FORMULA exp(-(arg_fun[0]*arg_fun[0] + arg_fun[1]*arg_fun[1])/800.0)/(800.0 * M_PI)

// interval of integration. Lower half of the array contains the lower bounds
// Upper half contains higher bounds
#define INT_INTERVALS {-1.000000e+02,1.000000e+02,-1.000000e+02,1.000000e+02}

//#define INT_OUT_FILEPATH "datareport" \\ to save data in dataport uncomment this

// must be filled with INT_DIMENSIONS powers of INT_BINS 
#define INT_DIV_NUMBERS {1,256}

// the total number of threads.must be equal to INT_BINS^INT_DIMENSIONS.
#define INT_TOTAL_BINS 65536LL

// dimensions of the bin
#define INT_BIN_DIMS {2.000000e+02,2.000000e+02}

// ipervolume of the integration interval
#define INT_VOLUME 4.000000e+04

#define INT_BIN_IPERVOLUME (INT_VOLUME/INT_TOTAL_BINS)

// number of bins per interval
#define INT_BINS 256LL

// points per thread per sample-mean and proportional factor of the same quantity
// for vegas
#define INT_ELMUL 1000LL

#endif
