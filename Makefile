
SHADER_VERSION = -arch=sm_20
CC = gcc-4.6
NVCC = nvcc
CFLAGS = -Wall
CFLAGS_NVCC  = -DINT_CUDA_SOURCE $(SHADER_VERSION) -dc --compiler-bindir=/usr/bin/$(CC) 
CFLAGS_NVCC_DLINK = $(SHADER_VERSION) -dlink

##CUDA_LIB_PATH   = /usr/lib/i386-linux-gnu/
##CUDA_LIB_PATH = /usr/local/cuda/lib64/
CUDA_LIB_PATH = /ust/local/lib/

LD = gcc-4.6
LDFLAGS_CC      = -lm     
LDFLAGS_NVCC    = -lm -L$(CUDA_LIB_PATH) -lcudart -lcurand -lcuda 

.PHONY: all clean_int_o clean_o clean sample_mean clean

all: sample_mean miser vegas

sample_mean: sample_mean.cu.x
	cp sample_mean.cu.x sample_mean
	-@rm -rf *.x 2>/dev/null || true

miser: miser.cu.x
	cp miser.cu.x miser
	-@rm -rf *.x 2>/dev/null || true

vegas: vegas_thread16.cu.x
	cp vegas_thread16.cu.x vegas
	-@rm -rf *.x 2>/dev/null || true

aux_cuda_func.o : aux_cuda_func.cu
	$(NVCC) $(CFLAGS_NVCC) $(REAL_CFLAG)  -o aux_cuda_func.o  aux_cuda_func.cu

aux_func.o : aux_func.c
	$(CC) $(CFLAGS) $(REAL_CFLAG) -c -o aux_func.o  aux_func.c

%.c.o  : %.c
	$(CC) $(CFLAGS) $(REAL_CFLAG) -c -o $@ $< 

%.cu.o : %.cu
	$(NVCC) $(CFLAGS_NVCC) $(REAL_CFLAG) -o $@ $< 

%.c.x   : %.c.o aux_func.o aux_c_func.o
	$(LD) $(LDFLAGS_CC) -o $@ $< aux_func.o aux_c_func.o

%.cu.x   : %.cu.o aux_func.o aux_cuda_func.o
	$(NVCC) $(CFLAGS_NVCC_DLINK) $< aux_cuda_func.o -o dlink.o 
	$(LD) $(LDFLAGS_NVCC) -o $@ $< aux_func.o aux_cuda_func.o dlink.o

clean_int_o 	:
	-@rm -rf *.c.o	2>/dev/null || true
	-@rm -rf *.cu.o 2>/dev/null || true

clean_o :
	-@rm -rf *.o 2>/dev/null || true
 
clean	:  clean_int_o clean_o clean_x
	-@rm -r sample_mean vegas miser 2>/dev/null || true

