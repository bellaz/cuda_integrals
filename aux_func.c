/*
*    aux_func.c
*    Copyright (C) 2014  Bellandi Andrea
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "integral_interchange.h"

const char *error_strings[] = {	"STATUS_OK",
				"STATUS_ERROR",
				"STATUS_CUDA_ERROR",
				"STATUS_MALLOC_ERROR",
				"STATUS_MATH_ERROR",
				"STATUS_NO_DESIRED_STD",
				"STATUS_TIME_ERROR"};


const char * int_error_msg(unsigned char err){
  int i;
  for(i = 6; i > 0; i++){
    if ((err) & (0x01 << (i-1))) break;
  }

  return error_strings[i - 1];
}



void format_to_out(struct integral_return iret){

#ifndef INT_OUT_FILEPATH
	
    printf("Final results:\n    integral value: %e \n    integral error: %e\n    elapsed time(ms): %lld\n\n", iret.result, iret.err, iret.msec);
    printf("total number of passes: %d\n", iret.number_of_passes);
    printf("total number of function points %lld\n", iret.number_of_points);
   
  
#else

FILE * out_file = fopen(INT_OUT_FILEPATH,"a");

fprintf(out_file, " %le %le %lld %lld\n", iret.result, iret.err, iret.number_of_points, iret.msec);

fflush(out_file); 
	  

#endif
}

void test_time_error(int error, struct integral_return *ir, int line, char * file){

  if(error == -1){
    ir->error = STATUS_ERROR | STATUS_TIME_ERROR;
    printf("time error %d (errno) called at %d line of %s: %s", errno, line, file, strerror(errno));
  }
}

void test_malloc_error(void * p, struct integral_return *ir, int line, char * file){
  
  if(p == NULL){
    ir->error = STATUS_ERROR | STATUS_MALLOC_ERROR;
    printf("malloc error %d (errno) called at %d line of %s: %s", errno, line, file, strerror(errno));
  }
}

long timevaldiff(struct timeval *starttime, struct timeval *finishtime)
{
  long msec;
  msec=(finishtime->tv_sec-starttime->tv_sec)*1000;
  msec+=(finishtime->tv_usec-starttime->tv_usec)/1000;
  return msec;
}


struct ndim_point_l get_coords_global(long long int idx){

  
  int i;
  const long long int div_numbers[INT_DIMENSIONS] = INT_DIV_NUMBERS;

  struct ndim_point_l result;
  for(i = 0; i < INT_DIMENSIONS; i++) result.point[i] = ((idx / div_numbers[i]) % INT_BINS);
  
  return result;
}

struct ndim_point eval_bin1D_global(long long int val){

  struct ndim_point result;
  double tmp;
  int i;

  const double integral_intervals[INT_DIMENSIONS * 2]  = INT_INTERVALS;
  const double bin_dims[INT_DIMENSIONS] = INT_BIN_DIMS;
  const long long int div_numbers[INT_DIMENSIONS] = INT_DIV_NUMBERS;

  for(i = 0; i < INT_DIMENSIONS; i++)
    { 
      tmp = bin_dims[i] * ((val / div_numbers[i]) % INT_BINS)/INT_BINS;
      result.point[i] = integral_intervals[2 * i] + tmp;
    }

  return result;
}


double eval_func_global(struct ndim_point val){

  int i;
  double arg_fun[INT_DIMENSIONS];
  for(i = 0; i < INT_DIMENSIONS; i++) arg_fun[i] = val.point[i];

  return INT_FORMULA;
}


double theta_global(double x){

  return ((x > 0.0) ? 1.0 : 0.0);
}

double roundzero_global(double x){

  return ((x > 0.0) ? x : 0.0);
}



int main(){  

  struct integral_return ret;
  memset(&ret, 0x00, sizeof(struct integral_return));
  ret = integral_function(ret);
  format_to_out(ret);

  return EXIT_SUCCESS;
}
