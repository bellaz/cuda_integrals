/*
*    VEGAS integration module
*    Copyright (C) 2014  Bellandi Andrea
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "integral_interchange.h"

#define BLOCK_SIZE 64
#define MAX_BLOCKS 1024

#define MAX_PASSES 256
#define MIN_PASSES 5
#define TINY 1.0e-30
#define BIG 1.0e30
#define ALPH 1.5
#define WARP_STRIDE 16

//#define CUDA_RAND_TYPE curandStateMRG32k3a_t 
//#define CUDA_RAND_TYPE curandStatePhilox4_32_10_t  
#define CUDA_RAND_TYPE curandStateXORWOW_t 

#define MAX(x,y) (x > y) ? x : y;
#define MIN(x,y) (x < y) ? x : y;

//#define PRINT_INTERVALS

struct integral_return * integral_result_p;

struct vegas_data{

  double mean;
  double var;
};

double * dev_mean;
double * dev_var;
double * dev_inter_grid1d;
__constant__ double dev_inter_gridnd[INT_DIMENSIONS - 1][INT_BINS + 1];
double * dev_wgt_grid;

CUDA_RAND_TYPE * dev_rand_states;

__global__ void initialize_random_gens(CUDA_RAND_TYPE * rand_states, long int seed){

  CUDA_RAND_TYPE state;
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  curand_init(seed, index, 0, &state);
  rand_states[index] = state; 

}

__global__ void integration_cuda(double * mean, double * var, double * inter_grid1d, double * wgt_grid,CUDA_RAND_TYPE * rand_states, long long int crelmul,long long int offset){

  __shared__ double summ[BLOCK_SIZE];
  __shared__ double summ2[BLOCK_SIZE];

  double bindim;
  double fval;

  CUDA_RAND_TYPE state;

  int tid = threadIdx.x;
  int halfw = threadIdx.x / WARP_STRIDE;
  int wid = threadIdx.x % WARP_STRIDE;
 
  long long int aidx = blockIdx.x * BLOCK_SIZE + tid;
  long long int idx = aidx + offset;
  long long int mul;

  struct ndim_point_l ipoint;
  struct ndim_point   epointlow;
  struct ndim_point   epointivar;
  struct ndim_point   val_point;
  int i;

  summ[tid] = 0.0;
  summ2[tid] = 0.0;
  

  __syncthreads();

  if(idx < INT_TOTAL_BINS){
  
    state  = rand_states[aidx];
    ipoint = get_coords(idx);
          
    epointlow.point[0] = inter_grid1d[ipoint.point[0]];
    epointivar.point[0] = inter_grid1d[ipoint.point[0] + 1];
    epointivar.point[0] -= epointlow.point[0]; 

    bindim = epointivar.point[0];

    for(i = 0; i < INT_DIMENSIONS - 1; i++ ){
      
      epointlow.point[i + 1] = dev_inter_gridnd[i][ipoint.point[i + 1]];
      epointivar.point[i + 1] = dev_inter_gridnd[i][ipoint.point[i + 1] + 1];
      epointivar.point[i + 1] -= epointlow.point[i + 1]; 

      bindim *= epointivar.point[i + 1]; 
    }

    for(mul = 0; mul < crelmul; mul ++){
      for(i = 0; i < INT_DIMENSIONS; i++ ){

	val_point.point[i] = curand_uniform_double(&state);
	val_point.point[i] *= epointivar.point[i];
	val_point.point[i] += epointlow.point[i];
      }                                         

      fval = eval_func(val_point);
      summ[tid]  +=  fval;
      summ2[tid] += (fval * fval);
    }

    summ[tid] /= crelmul;
    summ2[tid] = (summ2[tid] - (summ[tid] * summ[tid] / crelmul))/(crelmul * crelmul);
    summ2[tid] = MAX(TINY, summ2[tid]);

    summ[tid]  *= bindim;
    summ2[tid] *= bindim * bindim;
      
    atomicAdd(&(wgt_grid[ipoint.point[0]]), fabs(summ[tid])); 

    rand_states[aidx] = state;
  }

  
  __syncthreads();
  
  for(i = WARP_STRIDE/2; i>0; i>>=1) {
      if(wid < i){
	summ[halfw * WARP_STRIDE + wid] += summ[halfw * WARP_STRIDE + wid + i];
	summ2[halfw * WARP_STRIDE + wid] += summ2[halfw * WARP_STRIDE + wid + i];
      }
      __syncthreads();
  }

  for(i = wid; i < (INT_DIMENSIONS - 1); i += WARP_STRIDE){
    atomicAdd(&(wgt_grid[(i + 1) * INT_BINS + ipoint.point[i + 1]]), fabs(summ[halfw * WARP_STRIDE]));
  }

  __syncthreads();

  if(tid == 0){

    for(i = 1; i < BLOCK_SIZE/WARP_STRIDE; i++) {
      summ[0] += summ[i * WARP_STRIDE];
      summ2[0] += summ2[i * WARP_STRIDE];   
    }
    atomicAdd(mean, summ[0]);
    atomicAdd(var, summ2[0]);
  }
}



double host_inter_grid[INT_DIMENSIONS][INT_BINS + 1];
const double intervals[INT_DIMENSIONS * 2] = INT_INTERVALS;

void vegas_init();
void vegas_get_grid(double * grid);
struct vegas_data vegas_pass(long long int crelmul);
void * integration_pthread(void * params);
void vegas_refine_grid();
void vegas_close();


struct integral_return integral_function(struct integral_return integral_result)
{

  #ifdef PRINT_INTERVALS
  
  long long int j;
  double grid[INT_DIMENSIONS * (INT_BINS + 1)];

  #endif

  double mean2fvar_sum = 0.0;
  double mean3fvar_sum = 0.0;
  double result;
  double std;
  double chisq;
  int error;
  int passes = 0;
  int i;
  struct timeval msec_init, msec_end;
  struct vegas_data results[MAX_PASSES];
 
  integral_result.error = STATUS_OK;

  if(INT_BINS % WARP_STRIDE != 0){

    printf("error, BINS must be a multiple of %d\n\n", WARP_STRIDE);
    integral_result.error = STATUS_ERROR;
    CHECK_ERRORS(integral_result)	
  }

  if(INT_DIMENSIONS < 2){

    printf("error, DIMENSIONS must be > 1 \n\n", WARP_STRIDE);
    integral_result.error = STATUS_ERROR;
    CHECK_ERRORS(integral_result)	
  }


  error = gettimeofday(&msec_init,NULL);
  test_time_error(error, &integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)	
    
  integral_result_p = &integral_result;

  vegas_init();


  do{
  #ifdef PRINT_INTERVALS
  
  vegas_get_grid(grid);

  printf("\nPass %d:", passes);
  for(i = 0; i < INT_DIMENSIONS; i++){
    
    printf("\n\t dim %d:", i);
    for(j = 0; j < INT_BINS + 1 ; j++){
      printf(" %e", grid[i * (INT_BINS + 1) + j]);
    }
  }
  printf("\n");

  #endif

  results[passes] = vegas_pass(INT_ELMUL);
  vegas_refine_grid();
  
  mean2fvar_sum += results[passes].mean * results[passes].mean / results[passes].var;
  mean3fvar_sum += results[passes].mean * results[passes].mean * results[passes].mean / results[passes].var;

  passes++;
  }
  while(passes < MIN_PASSES);

  do{

#ifdef PRINT_INTERVALS

vegas_get_grid(grid);

 printf("\nPass %d:", passes);
  for(i = 0; i < INT_DIMENSIONS; i++){
    
    printf("\n\t dim %d:", i);
    for(j = 0; j < INT_BINS + 1 ; j++){
      printf(" %e", grid[i * (INT_BINS + 1) + j]);
    }
  }
  printf("\n");

#endif

    results[passes] = vegas_pass(INT_ELMUL * 5);
    vegas_refine_grid();

    mean2fvar_sum += results[passes].mean * results[passes].mean / results[passes].var;
    mean3fvar_sum += results[passes].mean * results[passes].mean * results[passes].mean / results[passes].var;
    
    result = mean3fvar_sum  / mean2fvar_sum;
    std = result / sqrt(mean2fvar_sum); 
    
    passes++;

    chisq = 0.0;

    for(i = 0; i < passes; i++){
      // chisq += (results[i].mean - result) * (results[i].mean - result) / results[i].var; 
     chisq += (results[i].mean - result) * (results[i].mean - result) * results[i].mean * results[i].mean / ((result * result) * results[i].var); 
    
    }

    chisq /= (double)(passes - 1);

  }
  while(chisq > 1.5);

  integral_result.result = result;
  integral_result.err = std;
  
  vegas_close();
  
  error = gettimeofday(&msec_end,NULL);
  test_time_error(error, &integral_result, __LINE__, __FILE__);
  CHECK_ERRORS(integral_result)
    
  
  integral_result.msec = timevaldiff(&msec_init, &msec_end);
  
  integral_result.number_of_passes = passes;
  
  integral_result.number_of_points = INT_TOTAL_BINS * INT_ELMUL * (MIN_PASSES + 5 * (passes - MIN_PASSES));

  return integral_result;
}


void vegas_init(){


  int i;
  long long int j;
  double tmp;

  cudaMalloc(&dev_rand_states, sizeof(CUDA_RAND_TYPE) * BLOCK_SIZE * MAX_BLOCKS);
  cudaMalloc(&dev_mean, sizeof(double));
  cudaMalloc(&dev_var, sizeof(double));
  cudaMalloc(&dev_inter_grid1d, sizeof(double) * (INT_BINS + 1));
  cudaMalloc(&dev_wgt_grid, sizeof(double) * INT_DIMENSIONS * INT_BINS);
  

  for(i = 0; i < INT_DIMENSIONS; i++){
    for(j = 0; j < INT_BINS + 1; j++){

      tmp = (intervals[2 * i + 1] - intervals[2 * i]); 
      tmp *= ((double) j/(double) INT_BINS);
      host_inter_grid[i][j] = tmp + intervals[2 * i];
    }

  }

  cudaMemcpy(dev_inter_grid1d, host_inter_grid, sizeof(double) * (INT_BINS + 1), cudaMemcpyHostToDevice);
  cudaMemcpyToSymbol(dev_inter_gridnd,  &(host_inter_grid[1][0]), sizeof(double) * (INT_DIMENSIONS - 1) * (INT_BINS + 1));

  initialize_random_gens<<< MAX_BLOCKS, BLOCK_SIZE >>>(dev_rand_states, time(NULL));

  cudaDeviceSynchronize();
}

void vegas_get_grid(double * grid){
  
   memcpy(grid, host_inter_grid, sizeof(double) * INT_DIMENSIONS * (INT_BINS + 1));
}

struct vegas_data vegas_pass(long long int crelmul){

  struct vegas_data result;
  long long int offset;
  long long int nblocks;

  cudaMemset((void*)dev_mean  , 0x00, sizeof(double));
  cudaMemset((void*)dev_var  , 0x00, sizeof(double));
  cudaMemset((void*)dev_wgt_grid  , 0x00, sizeof(double) * INT_DIMENSIONS * INT_BINS);
  
  for(offset = 0; offset < INT_TOTAL_BINS; offset += MAX_BLOCKS * BLOCK_SIZE){
  
    nblocks = MIN(MAX_BLOCKS, (INT_TOTAL_BINS - offset - 1)/BLOCK_SIZE + 1);

    integration_cuda<<< nblocks, BLOCK_SIZE >>>(dev_mean, dev_var, dev_inter_grid1d, dev_wgt_grid, dev_rand_states, crelmul, offset);

    cudaDeviceSynchronize();
  }
  
  cudaMemcpy(&(result.mean), dev_mean , sizeof(double), cudaMemcpyDeviceToHost);
  cudaMemcpy(&(result.var), dev_var, sizeof(double), cudaMemcpyDeviceToHost);
  
  return result;
}



void vegas_refine_grid(){

  double wgt_grid[INT_DIMENSIONS][INT_BINS];

  double int_wgts[INT_DIMENSIONS];
  double aux_grid[INT_BINS - 1];
  double tmp;
  int i, k;

  long long int j;
  
  double xn;
  double xo;
  double dr;
  double lg;

  cudaMemcpy( wgt_grid, dev_wgt_grid, INT_DIMENSIONS * INT_BINS * sizeof (double), cudaMemcpyDeviceToHost);

  for(i = 0; i < INT_DIMENSIONS; i++){

    int_wgts[i] = 0.0;

    for(j = 0; j < INT_BINS; j++){
      int_wgts[i] += wgt_grid[i][j];
    }

    for(j = 0; j < INT_BINS; j++){
      
      tmp = wgt_grid[i][j]/int_wgts[i];
     
      wgt_grid[i][j] = pow((tmp - 1.0)/(log(fmax(TINY, tmp))), ALPH);
      //wgt_grid[i][j] = pow(acos(1.0 - tmp),ALPH);
      //wgt_grid[i][j] = pow(tmp,ALPH * 0.45);
      //wgt_grid[i][j] = pow((0.2 + tmp * 0.8), ALPH);
    }

    int_wgts[i] = 0.0;

    for(j = 0; j < INT_BINS - 1; j++){
      wgt_grid[i][j + 1] += wgt_grid[i][j];
    }
    
    int_wgts[i] = wgt_grid[i][INT_BINS - 1] / INT_BINS;
  }

  for(i = 0; i < INT_DIMENSIONS; i++){

    k = 0;
    xn = 0.0;
    xo = 0.0;
    dr = int_wgts[i];
    lg = 0.0;

    for(j = 0;j < INT_BINS - 1; j++){
      
      while(dr > wgt_grid[i][k]) k++;

      if(k > 0){
	lg = wgt_grid[i][k - 1];
      }
      
      xo = host_inter_grid[i][k];
      xn = host_inter_grid[i][k + 1] - xo;
      aux_grid[j] = xo + xn * (dr - lg)/(wgt_grid[i][k] - lg); 
      dr += int_wgts[i];

    }

    for(j = 0;j < INT_BINS - 1; j++){
      host_inter_grid[i][j + 1] = aux_grid[j];
    }

  }

  cudaMemcpy(dev_inter_grid1d, host_inter_grid, sizeof(double) * (INT_BINS + 1), cudaMemcpyHostToDevice);

  cudaMemcpyToSymbol(dev_inter_gridnd,  &(host_inter_grid[1][0]), sizeof(double) * (INT_DIMENSIONS - 1) * (INT_BINS + 1));

}


void vegas_close(){


  cudaFree(dev_rand_states);
  cudaFree(dev_mean);
  cudaFree(dev_var);
  cudaFree(dev_inter_grid1d);
  cudaFree(dev_wgt_grid);

}
